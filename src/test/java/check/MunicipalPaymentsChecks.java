package check;

import com.codeborne.selenide.ElementsCollection;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.attribute;

/**
 * Created by A.G.Belyaev on 02.09.2018.
 */
public class MunicipalPaymentsChecks {

    @Step("Проверяем, что первый поставщик услуг - {1}")
    public static void checkFirstProviderName(ElementsCollection providersList, String providerName) {
        providersList.first().shouldHave(attribute("title", providerName));
    }
}
