package common;

/**
 * Created by A.G.Belyaev on 02.09.2018.
 */
public class Commons {

    //Common XPaths
    public static String PAYMENTS_SELECTOR_BASE_PAGE = "//a[contains(text(),'�������')]";
    public static String MUNICIPAL_PAYMENTS_SELECTOR_BASE_PAGE = "//div[contains(text(),'���')]";
    public static String CURRENT_DISTRICT_SELECTOR = "//span[@data-qa-file='Link']";
    public static String DEFAULT_DISTRICT_SELECTOR = "//*[text()='�. ������']";
    public static String MUNICIPAL_PROVIDERS_LIST_SELECTOR = "//section[@data-qa-file='UILayoutSection']//a[@data-qa-file='Link']";


    //Common strings
    public static String BASE_URL = "https://tinkoff.ru";
    public static String DEFAULT_DISTRICT = "������";
    public static String DEFAULT_PROVIDER = "���-������";
}
