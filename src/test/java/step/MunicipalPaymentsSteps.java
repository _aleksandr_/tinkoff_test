package step;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static common.Commons.*;

/**
 * Created by A.G.Belyaev on 02.09.2018.
 */
public class MunicipalPaymentsSteps {

    @Step("Переходим в раздел оплаты ЖКХ")
    public static void openMPayments() {
        $(By.xpath(PAYMENTS_SELECTOR_BASE_PAGE)).click();
        $(By.xpath(MUNICIPAL_PAYMENTS_SELECTOR_BASE_PAGE)).click();
    }

    @Step("Ставим текущим регионом Москву, если не выбрана")
    public static void setMoscow() {
        SelenideElement currentDistrict = $(By.xpath(CURRENT_DISTRICT_SELECTOR));
        if (!currentDistrict.getText().contains(DEFAULT_DISTRICT)) {
            currentDistrict.click();
            $(By.xpath(DEFAULT_DISTRICT_SELECTOR)).click();
        }
    }

    @Step("Получаем список поставщиков услуг")
    public static ElementsCollection getProvidersList() {
        return $$(By.xpath(MUNICIPAL_PROVIDERS_LIST_SELECTOR));
    }

    @Step("Выбираем первого поставщика услуг")
    public static void chooseFirstProvider(ElementsCollection providersList) {
        providersList.first().click();
    }
}
