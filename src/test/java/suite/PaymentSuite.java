package suite;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static check.MunicipalPaymentsChecks.checkFirstProviderName;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static common.Commons.BASE_URL;
import static common.Commons.DEFAULT_PROVIDER;
import static step.MunicipalPaymentsSteps.*;

/**
 * Created by A.G.Belyaev on 02.09.2018.
 */
public class PaymentSuite {

    @BeforeClass
    public static void beforeTest() {
        Configuration.startMaximized=true;
        Configuration.browser = "chrome";
        Configuration.timeout=6000;
        open(BASE_URL);
    }

    @Test
    public void firstTest() {
        openMPayments();
        setMoscow();
        ElementsCollection providersList = getProvidersList();
        checkFirstProviderName(providersList, DEFAULT_PROVIDER);
        chooseFirstProvider(providersList);
        $(By.linkText("�������� ��� � ������"));
    }

}
